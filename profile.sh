[ "$BASH_VERSION" != "" ] || return 0
[ -f /run/.toolboxenv ] && return 0

function command_not_found_handle {
	toolbox-command-not-found $@
}
export -f command_not_found_handle
