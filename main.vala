// toolbox-command-not-found: Runs commands in the toolbox if not found on the host
// It also transparently passes through package managers without sudo

const string GROUP = "command-not-found";
const string KEY_ENABLED = "enabled";
const string KEY_CONTAINER = "container";
const string KEY_COMMANDS = "commands";
const string KEY_BLOCKED = "blocked_commands";
const int FALLBACK_RETURN = 127;

int main(string[] args) {
	if (args.length < 2) {
		error("Not enough arguments.");
	}
	string command = args[1];
	bool is_packagemanager = Regex.match_simple("(dnf|apt)", command);

	// Load the settings
	string filename = Path.build_filename(
		Environment.get_user_config_dir(),
		"toolbox", "command-not-found.conf"
	);
	KeyFile settings = new KeyFile();
	bool loaded = false, enabled = true;
	string container = "default";
	string[] commands = {}, blocked = {};
	try {
		// Load the file if possible
		try {
			loaded = settings.load_from_file(filename, KeyFileFlags.NONE);
		} catch (FileError ignored) {}

		// Populate the defaults
		if (!loaded) {
			// Set the default keys of the file
			settings.set_boolean(GROUP, KEY_ENABLED, enabled);
			settings.set_string(GROUP, KEY_CONTAINER, container);
			settings.set_string_list(GROUP, KEY_COMMANDS, commands);
			settings.set_string_list(GROUP, KEY_BLOCKED, blocked);
			try_to_save(settings, filename);		
		}

		// Read the settings
		enabled = settings.get_boolean(GROUP, KEY_ENABLED);
		container = settings.get_string(GROUP, KEY_CONTAINER);
		commands = settings.get_string_list(GROUP, KEY_COMMANDS);
		blocked = settings.get_string_list(GROUP, KEY_BLOCKED);
	} catch (KeyFileError e) {
		warning("Error parsing settings: %s", e.message);
	}

	// If disabled, fallback to the default bash behavior
	if (!enabled || command in blocked)
		return fallback(command);

	// If the command isn't saved, prompt what to do with it
	if (!(command in commands)) {
		// If the command isn't in the container, don't even bother prompting
		if (!command_in_container(command, container))
			return fallback(command);
	
		print("Command `%s` only exists in the Toolbox.\n", command);
		print("Run it in the Toolbox? [Y/once/n/never]: ");
		string line = stdin.read_line();
		bool once = Regex.match_simple("[oO][nN][cC][eE]", line);
		bool block = Regex.match_simple("[nN][eE][vV][eE][rR]", line);
		bool ans = !Regex.match_simple("^[nN][oO]?", line);

		if (block) {
			// Add the command to the list of blocked commands
			settings.set_string_list(GROUP, KEY_BLOCKED, append_to_array(blocked, command));
			try_to_save(settings, filename);			
			return FALLBACK_RETURN;
		} else if (ans) {
			if (!once) {
				// Add the command to the list of known-good commands
				settings.set_string_list(GROUP, KEY_COMMANDS, append_to_array(commands, command));
				try_to_save(settings, filename);
			}
		} else return FALLBACK_RETURN;
	}

	// Build the toolbox command line and run the command
	int extra = (container != "default") ? 4 : 2;
	if (is_packagemanager) 
		extra++;
	string[] argv = new string[args.length + extra];
	int i = 0;
	argv[i++] = "toolbox";
	argv[i++] = "run";
	if (container != "default") {
		argv[i++] = "--container";
		argv[i++] = container;
	}
	if (is_packagemanager)
		argv[i++] = "sudo";
	for (int j = 1; j < args.length; j++)
		argv[i++] = args[j];
	argv[i++] = null;
	
	Posix.execvp("toolbox", argv);
	return 0;
}

// Tries to save the keyfile to the file path
void try_to_save(KeyFile settings, string filename) {
	try {
		settings.save_to_file(filename);
	} catch (FileError e) {
		warning("command-not-found: Couldn't save config file: %s", e.message);
	}
}

// Old default behavior of command-not-found
int fallback(string command) {
	print("%s: command not found\n", command);
	return FALLBACK_RETURN;
}

// Returns a new array with append added to the end of it
string[] append_to_array(string[] old, string append) {
	string[] out = new string[old.length + 1];
	int i;
	for (i = 0; i < old.length; i++)
		out[i] = old[i];
	out[i] = append;
	return out;
}

// Returns true if the command exists in the container, false otherwise
bool command_in_container(string command, string container) {
	string contarg = (container != "default") ? "--container %s".printf(container) : "";
	string cmdline = "toolbox run %s command -v %s".printf(contarg, command);
	string stdout, stderr;
	int status;
	try {
		Process.spawn_command_line_sync(cmdline, out stdout, out stderr, out status);
		return (status == 0) && (stdout.length != 0);
	} catch (SpawnError e) {
		return false;
	}
}
