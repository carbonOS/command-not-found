# comand-not-found

This is a small program that utilizes `toolbox` to make Bash's command-not-found behavior smarter.

If the user attempts to run a command that is not found on the host system, and if the command exists
in the user's default (or as-configured, see docs on configuring this tool) toolbox, then the command
will be run transparently in the toolbox.

This tool will also transparently add `sudo` to package manager commands (like `dnf` or `apt`),
so that it can install packages in the toolbox environment. This does not give these commands
any real superuser access.

## Example:

```
$ gcc
gcc: command not found
$ dnf install gcc
dnf: command not found
$ toolbox create
[...]
$ dnf install gcc
Command `dnf` only exists in the Toolbox.
Run it in the Toolbox? [Y/once/n/never]: once
[...]
$ dnf
Command `dnf` only exists in the Toolbox.
Run it in the Toolbox? [Y/once/n/never]: n
$ gcc
Command `gcc` only exists in the Toolbox.
Run it in the Toolbox? [Y/once/n/never]: y
[...]
$ gcc
[...]
```

## Configuring

command-not-found stores it's configuration in `~/.config/toolbox/command-not-found.conf`. This is a simple keyfile,
and trying to run an nonexistant command once will generate it with default values. Here's what's configurable:

`enabled`: whether or not the toolbox behavior should be enabled, or if command-not-found should behave like bash normally does

`container`: The toolbox container to operate on. Special value `default` let's toolbox decide on a container itself

`commands`: A list of commands that will be run without prompting. Automatically updated to include anything you answered yes to at the prompt

`blocked_commands`: A list of commands that should always fall back to standard behavior. Automatically updated to include anything you answer 
never to at the prompt
